# docker-in-docker using a self hosted runner and the VM daemon

Examples of docker in docker usage with a self hosted runner (on cloudstack for example). In this example we will use the docker deamon of the VM.

## General configuration

To run this project you first have to:

1. [Enable project CI/CD feature](https://gitlab.inria.fr/gitlabci_gallery#enable-shared-runners)
2. [Enable container registry](https://gitlab.inria.fr/gitlabci_gallery#enable-container-registry).

## Register the project runner

Then you have to define a new project runner in GitLab:

- On the left sidebar in the Gitlab interface, go to **Settings** → **CI/CD**. If you don't have a **CI/CD** item, then you should [enable project CI/CD feature](https://gitlab.inria.fr/gitlabci_gallery#enable-shared-runners)  first.
- Expand **Runners**.
- Click on **New project runner** button.
- Choose **Linux** as OS and set the tags you want for your runner. We set **VM daemon, docker, linux** for this project. 
- Click **Create runner** and copy the token in a safe place.

Connect to your VM and:
- Install gitlab runner. If you use a Cloudstack VM you can follow the instructions given in the [CI documentation](https://inria-ci.gitlabpages.inria.fr/doc/page/gitlab/#installation-example-on-a-gnulinux-slave-from-ciinriafr-ci-user-account). Else you can refer to the [GitLab documentation(https://docs.gitlab.com/runner/install/)].
- [Install docker](https://docs.docker.com/get-docker/).
- Register the runner as sudo with the following command:
```bash
sudo gitlab-runner register --url https://gitlab.inria.fr --token YOUR-TOKEN
```
    - choose **docker** as executor
    - choose **docker:24.0.5** as default Docker image
- Check your runner have beeen successfully registered with `gitlab-runner run` command. Your runner must appear with a green point on  **Settings** → **CI/CD** → **Runners**.

### Setting your runner to access docker daemon:

To do so we have to modify the runner configuration file to mount the VM daemon socket in the runner.
Connect your VM as sudo and open the config file: `sudo vi /etc/gitlab-runner/config.toml` file. Add `"/var/run/docker.sock:/var/run/docker.sock"` in the volumes option of 
your runner:

```toml
[[runners]]
  name = "docker-in-docker-VM"
  url = "https://gitlab.inria.fr"
  id = 7293
  token = XXXX
  token_obtained_at = 2024-03-14T16:18:17Z
  token_expires_at = 0001-01-01T00:00:00Z
  executor = "docker"
  [runners.cache]
    MaxUploadedArchiveSize = 0
  [runners.docker]
    tls_verify = false
    image = "docker:24.0.5"
    privileged = false
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/var/run/docker.sock:/var/run/docker.sock","/cache"]
    shm_size = 0
    network_mtu = 0
```

Restart the gitlab-runner service (`sudo gitlab-runner restart`).

The `.gitlab-ci.yml` file contains two jobs/stages that will be executed sequentially. Both are executed in the runner 
identified by its tags.

The first stage loads the `docker:24.0.5` image that has been specified as the default docker image (that is why we 
do specify the image name in the stage description). The VM daemon is used to build the docker image with the given 
Dockerfile. This image is then stored in the **Packages and registries** → **Container Registry**.

The second stage executes a command in the docker container.
